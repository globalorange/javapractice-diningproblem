package nl.stefanansing.dining;

public class Restaurant {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		long openTime = 10000;
		
		System.out.println("[RESTAURANT] The Restaurant is open for " + (openTime / 1000) + " seconds");
		
		Fork[] forks = new Fork[5];
		for(int i = 0; i < forks.length; i++) {
			forks[i] = new Fork();
		}
		
		Philosopher plato = new Philosopher(forks[0], forks[1], true, "Plato");
		Philosopher voltaire = new Philosopher(forks[1], forks[2], false, "Voltaire");
		Philosopher rene = new Philosopher(forks[2], forks[3], false, "Ren� Descartes");
		Philosopher socrates = new Philosopher(forks[3], forks[4], true, "Socrates");
		Philosopher stefan = new Philosopher(forks[4], forks[0], false, "Stefan");
		
		plato.start();
		voltaire.start();
		rene.start();
		socrates.start();
		stefan.start();
		
		try {
			Thread.sleep(openTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("[RESTAURANT] The Restaurant is Closing");
		
		plato.stop();
		voltaire.stop();
		rene.stop();
		socrates.stop();
		stefan.stop();
		
		boolean waiting = true;
		while(waiting) {
			if(!(plato.isRunning() || voltaire.isRunning() || rene.isRunning() || socrates.isRunning() || stefan.isRunning())) {
				waiting = false;
			} else {
				System.out.println("[RESTAURANT] Waiting for 1 second for the philosophers to stop eating or thinking.");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		System.out.println("[RESTAURANT] The Restaurant is Closed.");
		
	}

}
