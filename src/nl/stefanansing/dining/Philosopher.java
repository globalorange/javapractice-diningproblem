package nl.stefanansing.dining;

public class Philosopher implements Runnable {
	
	private static final long EAT_TIME = 5000;
	private static final long THINK_TIME = 5000;
	
	private Fork leftFork;
	private Fork rightFork;
	private boolean leftHanded;
	private String name;
	
	private long lastTimeEaten;
	
	private boolean run;
	private Object runLocker;
	
	private Thread thr;
	
	public Philosopher(Fork leftFork, Fork rightFork, boolean leftHanded, String name) {
		super();
		this.leftFork = leftFork;
		this.rightFork = rightFork;
		this.leftHanded = leftHanded;
		this.name = name;
		this.run = true;
		this.thr = null;
		this.runLocker = new Object();
		this.lastTimeEaten = System.currentTimeMillis();
	}

	@Override
	public void run() {
		boolean cont = true;
		while(cont) {
			//Check if the thread is supposed to stop
			synchronized (this.runLocker) {
				cont = this.run;
			}
			
			//When cont is set to false, the thread should stop immediately.
			if(!cont) {
				continue;
			}
			
			if(Thread.interrupted()) {
				continue;
			}
			
			this.think();
			
			if(Thread.interrupted()) {
				continue;
			}
			
			//We check on interruption within the take method. When the take method returns false it means that it failed.
			//So we should stop doing our stuff (And make sure to release it)
			if(this.leftHanded) {
				boolean leftTaken = this.leftFork.take(this);
				if(!leftTaken) { continue; }
				
				boolean rightTaken = this.rightFork.take(this);
				if(!rightTaken) { this.leftFork.release(this); continue; }
			} else {
				boolean rightTaken = this.rightFork.take(this);
				if(!rightTaken) { continue; }
				
				boolean leftTaken = this.leftFork.take(this);
				if(!leftTaken) { this.rightFork.release(this); continue; }
			}
			
			//He can eat now.
			this.eat();
			
			//Release the forks.
			if(this.leftHanded) {
				this.leftFork.release(this);
				this.rightFork.release(this);
			} else {
				this.rightFork.release(this);
				this.leftFork.release(this);
			}
		}
		
		System.out.println("[LEAVING] " + this.toString());
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Philosopher ");
		builder.append(this.name);
		return builder.toString();
	}
	
	private void think() {
		System.out.println("[THINKING] " + this.toString());
		try {
			Thread.sleep(THINK_TIME);
		} catch (InterruptedException e) {
			//Do nothing
		}
		//System.out.println("Philosopher " + this.name + " stopped thinking.");
	}
	
	private void eat() {
		if(this.leftFork.getHolder().equals(this) && this.rightFork.getHolder().equals(this)) {
			long now = System.currentTimeMillis();
			long delta = now - this.lastTimeEaten;
			System.out.println("[EATING][" + delta + "] " + this.toString());
			this.lastTimeEaten = now;
			try {
				Thread.sleep(EAT_TIME);
			} catch (InterruptedException e) {
				//Do nothing
			}
			//System.out.println("Philosopher " + this.name + " stopped eating.");
		} else {
			System.out.println("Philosopher " + this.name + " tried to eat, but he does not hold both forks.");
		}
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (leftHanded ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Philosopher other = (Philosopher) obj;
		if (leftHanded != other.leftHanded)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	public void start() {
		this.thr = new Thread(this);
		
		synchronized (this.runLocker) {			
			this.run = true;
		}
		
		thr.start();
	}
	
	public void stop() {
		synchronized (this.runLocker) {
			this.run = false;
			this.thr.interrupt();
		}
	}
	
	public boolean isRunning() {
		if(this.thr != null) {
			return this.thr.isAlive();
		}
		
		return false;
	}
	
	

}
