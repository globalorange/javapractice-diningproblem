package nl.stefanansing.dining;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Fork {
	
	private Lock locker;
	private Condition takeSignal;
	private Philosopher holder;
	
	public Fork() {
		this.locker = new ReentrantLock();
		this.takeSignal = this.locker.newCondition();
		this.holder = null;
	}
	
	public boolean take(Philosopher philosopher) {
		boolean result = true;
		try {
			//First lock the fork.
			this.locker.lock();
			
			//Check if it already has a holder, if so. Wait for the Take Signal
			//Which is given when the fork is released
			if(this.holder != null) {
				this.takeSignal.await();
			}
			
			//you have to lock so set your philosopher
			this.holder = philosopher;
			
		} catch (InterruptedException e) {
			result = false;
		} finally {
			this.locker.unlock();
		}
		
		return result;
	}
	
	public void release(Philosopher philosopher) {
		this.locker.lock();
		if(philosopher.equals(this.holder)) {
			this.holder = null;
		}
		this.takeSignal.signal();
		this.locker.unlock();
	}
	
	public Philosopher getHolder() {
		return this.holder;
	}

}
